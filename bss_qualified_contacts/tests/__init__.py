# -*- coding: utf-8 -*-
# Part of Qualified Contacts.
# See LICENSE file for full copyright and licensing details.

import test_partner_qualified_contact

checks = [
    test_partner_qualified_contact,
]
