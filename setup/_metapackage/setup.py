import setuptools

with open('VERSION.txt', 'r') as f:
    version = f.read().strip()

setuptools.setup(
    name="odoo10-addons-odoo10-addons-bss-partner",
    description="Meta package for odoo10-addons-bss-partner Odoo addons",
    version=version,
    install_requires=[
        'odoo10-addon-bss_email_whitelist',
        'odoo10-addon-bss_marital_status',
        'odoo10-addon-bss_partner_reference',
        'odoo10-addon-bss_qualified_contacts',
        'odoo10-addon-bss_resident_permit',
        'odoo10-addon-bss_split_partner_name',
        'odoo10-addon-bss_split_partner_name_name_first',
    ],
    classifiers=[
        'Programming Language :: Python',
        'Framework :: Odoo',
    ]
)
