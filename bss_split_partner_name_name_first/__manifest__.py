# -*- coding: utf-8 -*-
# Part of Split Partner Name - Name First.
# See LICENSE file for full copyright and licensing details.

{
    'name': 'Split Partner Name - Name First',
    'version': '10.0.1.2.0',
    "category": 'Bluestar/Generic module',
    'complexity': "easy",
    'description': """
Split Partner Name - Name First
===============================

An addon to "Split Partner Name" module, to use name first in computed name.
    """,
    'author': 'Bluestar Solutions Sàrl',
    'website': 'http://www.blues2.ch',
    'depends': ['bss_split_partner_name'],
    'data': ['views/res_partner_views.xml'],
    'demo': [],
    'test': [],
    'installable': True,
    'application': False,
    'auto_install': False,
    'images': [],
}
