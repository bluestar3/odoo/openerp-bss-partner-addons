# -*- coding: utf-8 -*-
# Part of Mail Whitelist.
# See LICENSE file for full copyright and licensing details.

{
    'name': 'Mail Whitelist',
    'version': '10.0.1.2.0',
    "category": 'Bluestar/Generic module',
    'complexity': "easy",
    'description': """
Mail Whitelist
==============
    """,
    'author': 'Bluestar Solutions Sàrl',
    'website': 'http://bluestar.solutions',
    'depends': ['mail'],
    'data': [],
    'demo': [],
    'test': [],
    'installable': True,
    'application': False,
    'auto_install': False,
}
